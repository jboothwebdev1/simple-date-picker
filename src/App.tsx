import React from 'react';
import './App.css';
import { DatePicker } from './lib';

function App() {
  return (
    <div className="App">
      <DatePicker callback={(date: any) => { console.log(date) }} theme={"dark"} />
    </div>
  );
}

export default App;
