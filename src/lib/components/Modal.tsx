import { useState } from 'react';
import { FaTimesCircle } from "react-icons/fa"
import "../styles/Modal.css"
import SelectionForm from './SelectionForm';
type refProp = {
  position: {
    x: number,
    y: number
  } | undefined,
  returnDate: Function,
  handleClose: Function,
  theme: string
}

/**
* The modal is responsible for creating the overlay and handling closing the form when the mouse is clicked outside of the form.
* @Params position: Position of the element that creating the modal.
* @Params returnDate: Function that returns the date to the application.
* @Params handleClose: Passed in function that allow the modal to close. 
* @Params Theme: Same as the theme for the date picker.
*/

export default function Modal({ position, returnDate, handleClose, theme }: refProp) {
  const width = 600
  const x = (document.body.clientWidth / 2) - (width / 2)

  const handleOverlayClick = (event: any) => {
    if (event.target.localName !== "svg" && event.target.localName !== "path") {
      if (event.target.className.includes("modal-container")) {
        handleClose()
      }
    }
  }

  return (
    <>
      <div className={`modal-container ${theme}`} id={"modal-container"} onClick={(event) => { handleOverlayClick(event) }} >
        <button className={"modal-close"} onClick={() => handleClose()}>
          <i className={"modal-icon"}><FaTimesCircle /></i>
        </button>
        <div className={`modal-inner ${theme}`} style={{ top: position?.y, left: x }} >
          <SelectionForm returnDate={returnDate} theme={theme} />
        </div>
      </div>
    </>
  )

}
