import { FaChevronDown, FaChevronUp } from "react-icons/fa"
import { MONTHS } from "../utils";
import "../styles/SelectionForm.css"
import { BaseSyntheticEvent, useState, MouseEvent } from "react";
import dayValidator from "../validators/dayValidator";
import Menu from "./Menu";

type selectionProps = {
  returnDate: Function,
  theme: string
}

/**
* Holds the components and the binding logic for building the date and sending it back.
* @Param ReturnDate: function used by the submit to pass the completed date back up the component tree.
*/
export default function SelectionForm({ returnDate, theme }: selectionProps) {
  const [dayIndex, setDayIndex] = useState(new Date().getDate())
  const [monthIndex, setMonthIndex] = useState(new Date().getMonth())
  const [yearIndex, setYearIndex] = useState(new Date().getFullYear())
  const [isOpen, setIsOpen] = useState(false)
  const [selection, setSelection] = useState('')
  const [position, setPosition] = useState({})
  const yearUpperLimit = yearIndex + 50
  const yearLowerLimit = yearIndex - 50

  const handleDayChange = (direction: string) => {
    setDayIndex(dayValidator(dayIndex, MONTHS[monthIndex].number, yearIndex, direction))
  }

  const handleMonthChange = (direction: string) => {
    if (direction === "up" && monthIndex < 11) {
      setMonthIndex(monthIndex + 1)
    } else if (direction === "down" && monthIndex > 0) {
      setMonthIndex(monthIndex - 1)
    }
  }

  const handleYearChange = (direction: string) => {
    if (direction === "up" && yearIndex < yearUpperLimit) {
      setYearIndex(yearIndex + 1)
    } else if (direction === "down" && yearIndex > yearLowerLimit) {
      setYearIndex(yearIndex - 1)
    }
  }

  const handleSubmit = (event: BaseSyntheticEvent) => {
    const createdDate = new Date(yearIndex, monthIndex, dayIndex)
    returnDate(createdDate)
  }

  const handleMenuOption = (selected: string, event: MouseEvent) => {
    if (!isOpen) {
      setSelection(selected)
      const currentPosition = {
        x: event.clientX,
        y: event.clientY
      }
      setPosition(currentPosition)
    }
  }

  return (
    <>
      <div className={"selection-outer"}>

        <div className={"selection-inner"}>
          <div onMouseEnter={(event) => handleMenuOption("day", event)} onClick={() => { setIsOpen(true) }}>
            <h3>
              {dayIndex}
            </h3>
          </div>

          <div className={"selection-section"}>
            <button className={`selection-arrow ${theme}`} onClick={() => { handleDayChange("up") }} >
              <FaChevronUp />
            </button>
            <button className={`selection-arrow ${theme}`} onClick={() => { handleDayChange("down") }}>
              <FaChevronDown />
            </button>
          </div>
        </div>

        <div className={"selection-inner month"}>
          <div onMouseEnter={(event) => handleMenuOption("month", event)} onClick={() => { setIsOpen(true) }}>
            <h3>
              {MONTHS[monthIndex].name}
            </h3>
          </div>

          <div className={"selection-section "}>
            <button className={`selection-arrow ${theme}`} onClick={() => { handleMonthChange("up") }}>
              <FaChevronUp />
            </button>
            <button className={`selection-arrow ${theme}`} onClick={() => { handleMonthChange("down") }}>
              <FaChevronDown />
            </button>
          </div>
        </div>

        <div className={"selection-inner year"}>
          <div onMouseEnter={(event) => { handleMenuOption("year", event) }} onClick={() => { setIsOpen(true) }}>
            <h3>
              {yearIndex}
            </h3>
          </div>

          <div className={"selection-section"}>
            <button className={`selection-arrow ${theme}`} onClick={() => { setYearIndex(yearIndex + 1) }}>
              <FaChevronUp />
            </button>
            <button className={`selection-arrow ${theme}`} onClick={() => { setYearIndex(yearIndex - 1) }}>
              <FaChevronDown />
            </button>
          </div>
        </div>

        <div className={"selection-inner"}>
          <button className={"submit"} onClick={(event) => handleSubmit(event)}>
            Submit
          </button>
        </div>
        {isOpen ? <Menu selection={selection} position={position} month={monthIndex} year={yearIndex} setIsOpen={setIsOpen} setDayIndex={setDayIndex} setMonthIndex={setMonthIndex} setYearIndex={setYearIndex} theme={theme} /> : <></>}
      </div>
    </>
  )
} 
