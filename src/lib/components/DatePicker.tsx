import "../styles/DatePicker.css"
import { FaCalendar } from "react-icons/fa"
import { useState } from "react"
import Modal from "./Modal"

type datePickerProps = {
  callback: Function,
  theme: string
}

/**
* Very simple date time picker for react application.
* @Param callback: needs to have a function that can use a standard JS datetime object
* the date picker will use this function to return the selected date. We reccomend a setState function. 
* @Param theme: currently only accepts "light" or "dark" as strings any other style changes will have to be done through className overrides.
* @Returns: common JS Date object.
*/
export default function DatePicker({ callback, theme }: datePickerProps) {
  const [isOpen, setIsOpen] = useState(false)
  const [position, setPosition] = useState<DOMRect>()
  let element: HTMLElement;

  const getCords = (element: HTMLElement) => {
    let elementPosition = element.getBoundingClientRect()
    setPosition(elementPosition)
  }

  const openModal = () => {
    if (position) {
      setIsOpen(true)
    }
  }

  const returnDate = (date: object) => {
    if (callback) {
      callback(date)
      setIsOpen(false)
    } else {
      console.warn("No setState call back present. Please past a setState callback to the date picker.")
    }
  }

  const handleClose = () => {
    setIsOpen(false)
  }


  return (
    <>
      <div className={`outer-container ${theme}`}>
        <div className={"inner-container"}>
          <p className={"title"}>Select Date</p>
          <button ref={el => { if (!el) return; element = el }} className={"select-button"}
            onMouseEnter={() => { getCords(element) }}
            onClick={() => { openModal() }}>
            <i className={`icon ${theme}`}><FaCalendar /></i>
          </button>
        </div>
      </div >
      {isOpen ? <Modal position={position} returnDate={returnDate} handleClose={handleClose} theme={theme} /> : <></>}
    </>
  )
}

