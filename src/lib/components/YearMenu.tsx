import { useEffect, useRef } from "react"
import '../styles/MonthMenu.css'

export default function YearMenu({ position, year, setIsOpen, setYearIndex, theme }: any) {
  const lowerLimit = year - 10
  const upperLimit = year + 10
  const yearArray = []

  for (let i = upperLimit; i > lowerLimit; i--) {
    yearArray.push(i)
  }

  return (
    <>
      <div
        className={`month-menu-container menu-${theme}`}
        style={{ top: position.y, left: position.x }}
      >
        {yearArray.map((yearI) => {
          return (
            <h5
              key={yearI}
              className={"month-menu-item"}
              onClick={() => {
                setYearIndex(yearI)
                setIsOpen(false)
              }}
            >
              {yearI}
            </h5>
          )
        })
        }
      </div>
    </>
  )
}
