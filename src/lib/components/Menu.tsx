import DayMenu from "./DayMenu";
import MonthMenu from "./MonthMenu";
import YearMenu from "./YearMenu";


/**
* The Menu is responsible for opening the correct sub menu for selections. 
* @Params selection: Used by the switch case to open the correct menu.
* @Params position: Position of the mouse click to open the menu at.
* @Params month: Currently selected month.
* @Params year: Currently selected year.
* @Params setIsOpen: State function to be able to close the menu.
* @Params setDayIndex: State function to change the selected day.
* @Params setMonthIndex: State function to change the selected month.
* @Params setYearIndex: State function to change the seledted year.
*/
export default function Menu({ selection, position, month, year, setIsOpen, setDayIndex, setMonthIndex, setYearIndex, theme }: any) {

  switch (selection) {
    case "day":
      return <DayMenu month={month} year={year} position={position} setIsOpen={setIsOpen} setDayIndex={setDayIndex} theme={theme} />
    case "month":
      return <MonthMenu setMonthIndex={setMonthIndex} position={position} setIsOpen={setIsOpen} theme={theme} />
    case "year":
      return <YearMenu position={position} year={year} setIsOpen={setIsOpen} setYearIndex={setYearIndex} theme={theme} />
    default:
      break;
  }
  return (
    <>
      <div>
      </div>
    </>
  )
}
