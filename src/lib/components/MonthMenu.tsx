import { MONTHS } from "../utils"
import "../styles/MonthMenu.css"

export default function MonthMenu({ setMonthIndex, position, setIsOpen, theme }: any) {
  return (
    <>
      <div
        className={`month-menu-container menu-${theme}`}
        style={{ top: position.y, left: position.y }}
      >
        {MONTHS.map((month) => {
          return (
            <h5
              key={month.number}
              className={"month-menu-item"}
              onClick={() => {
                setMonthIndex(month.number)
                setIsOpen(false)
              }}
            >
              {month.name}
            </h5>
          )
        })}
      </div>
    </>
  )
}
