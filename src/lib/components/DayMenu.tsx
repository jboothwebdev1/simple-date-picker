import "../styles/DayMenu.css"
import { useEffect, useState } from "react"
import { THIRTY, THIRTYDAYS, THIRTYONE, THIRTYONEDAYS } from "../utils"

export default function DayMenu({ month, year, position, setIsOpen, setDayIndex, theme }: any) {
  const [days, setDays] = useState<number[]>([])

  useEffect(() => {
    if (THIRTY.includes(month + 1)) {
      setDays(THIRTYDAYS)
    } else if (THIRTYONE.includes(month + 1)) {
      setDays(THIRTYONEDAYS)
    }
    // add the leap year to if statement
  }, [])


  return (
    <>
      <div
        className={`day-menu-container menu-${theme}`}
        style={{ top: position.y, left: position.x, zIndex: 200 }}>
        {days.map((day) => {
          return (
            <h5
              key={day}
              className={"day-menu-item"}
              onClick={() => { setDayIndex(day); setIsOpen(false) }}
            >{day}
            </h5>
          )
        })}
      </div>
    </>
  )
}
