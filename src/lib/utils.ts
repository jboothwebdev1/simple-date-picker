export const MONTHS = [
  {
    name: "January",
    number: 1
  },
  {
    name: "February",
    number: 2
  },
  {
    name: "March",
    number: 3
  },
  {
    name: "April",
    number: 4
  },
  {
    name: "May",
    number: 5
  },
  {
    name: "June",
    number: 6
  },
  {
    name: "July",
    number: 7
  },
  {
    name: "August",
    number: 8
  },
  {
    name: "September",
    number: 9
  },
  {
    name: "October",
    number: 10
  },
  {
    name: "November",
    number: 11
  },
  {
    name: "December",
    number: 12
  },
]

export const THIRTY = [4, 6, 9, 11]
export const THIRTYONE = [1, 3, 5, 7, 8, 10, 12]


export const THIRTYDAYS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30]
export const THIRTYONEDAYS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
export const TWENTYNINEDAYS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29]
