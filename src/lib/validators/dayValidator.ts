import { THIRTY, THIRTYONE } from "../utils"

export default function dayValidator(day: number, month: number, year: number, direction: string): number {
  let leapYear = checkForLeapYear(year)
  let valid = false
  let newDay = day
  const thirtyDays = THIRTY
  const thirtyOneDays = THIRTYONE

  // check for directrion of change 
  if (direction === 'up') {
    newDay += 1
  } else {
    newDay -= 1
  }
  if (month == 2 && leapYear && newDay <= 29 && newDay >= 1) {
    valid = true
  }
  else if (month == 2 && !leapYear && newDay <= 28 && newDay >= 1) { valid = true }
  else if (thirtyDays.includes(month) && newDay <= 30 && newDay >= 1) {
    valid = true
  } else if (thirtyOneDays.includes(month) && newDay <= 31 && newDay >= 1) {
    valid = true
  }

  // return the updated day if all checks pass. if not return the orginal day
  return valid ? newDay : day
}


function checkForLeapYear(year: number) {
  if (year % 4 === 0) {
    return true
  }
  return false
}


